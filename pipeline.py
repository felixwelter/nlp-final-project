import numpy as np
import utils
import data_providers
import preprocessors
import vectorizers
import clustering
import labeler
import sentiment

class Pipeline():
    def __init__(self, log_stdout=True):
        self.logging = log_stdout
        self.data_provider = None
        self.preprocessor = None
        self.vectorizer = None
        self.clusterer = None
        self.labeler = None
        self.sentiment_analyser = None
        self.visualizer = None
        self.notebook = False
    
    def show_in_notebook(self):
        self.notebook = True
    
    def execute(self):
        self.log("Loading tweets...")
        tweets = self.data_provider.load_data()
        self.log("Loaded {} tweets".format(len(tweets)))
        if self.preprocessor:
            self.log("Preprocessing tweets...")
            tweets = self.preprocessor.clean_tweets(tweets)
        else:
            self.log("No preprocessor set, skipping")
        self.log("Vectorizing tweets...")
        vecs = self.vectorizer.vectorize(tweets)
        self.log("Clustering tweets...")
        cluster_ids = self.clusterer.cluster(vecs)
        self.log("Summarize tweet clusters...")
        summaries = {}
        for cluster_id in np.unique(cluster_ids):
            cluster_tweets = np.array(tweets)[cluster_ids == cluster_id]
            summaries[cluster_id] = self.labeler.get_cluster_summary(cluster_tweets, 2)
            self.log("Summarized cluster {}".format(cluster_id))
        self.log("Analyzing sentiments...")
        sentiments = self.sentiment_analyser.get_sentiment(tweets)
        utils.visualize_clusters(cluster_ids, sentiments, summaries, notebook=self.notebook)
        
    def log(self, message):
        print(message) if self.logging else None
        
class Builder():
    def default_pipeline(self, topic):
        pipeline = Pipeline()
        pipeline.data_provider = data_providers.CachedSmallDataProvider(topic)
        pipeline.preprocessor = preprocessors.SurfaceCleaner()
        pipeline.vectorizer = vectorizers.FastTextMeanVectorizer()
        pipeline.clusterer = clustering.KMeansClustering(2)
        pipeline.labeler = labeler.LexRankSummarizer()
        pipeline.sentiment_analyser = sentiment.VaderSentimentAnalyser()
        return pipeline
    
    def elmo_pipeline(self, topic):
        pipeline = Pipeline()
        pipeline.data_provider = data_providers.CachedSmallDataProvider(topic)
        pipeline.preprocessor = preprocessors.SurfaceCleaner()
        pipeline.vectorizer = vectorizers.ElmoVectorizer()
        pipeline.clusterer = clustering.KMeansClustering(2)
        pipeline.labeler = labeler.LexRankSummarizer()
        pipeline.sentiment_analyser = sentiment.VaderSentimentAnalyser()
        return pipeline

    def top_ten_pipeline(self, topic):
        pipeline = Pipeline()
        pipeline.data_provider = data_providers.CachedSmallDataProvider(topic)
        pipeline.preprocessor = preprocessors.SurfaceCleaner()
        pipeline.vectorizer = vectorizers.FastTextMeanVectorizer()
        pipeline.clusterer = clustering.KMeansClustering(2)
        pipeline.labeler = labeler.LexRankSummarizer()
        pipeline.sentiment_analyser = sentiment.VaderSentimentAnalyser()
        pipeline.labeler = labeler.TopTenWords()
        return pipeline

if __name__ == "__main__":
    pipeline = Builder().default_pipeline("alexa")
    pipeline.execute()