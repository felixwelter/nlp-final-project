from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
from bokeh import plotting, layouts, io
from bokeh.models import widgets
import numpy as np

def scatter2d(vecs):
  pca = PCA(n_components=2)
  coords = pca.fit_transform(vecs)
  plt.scatter(coords[:,0], coords[:,1], s=1)

def visualize_clusters(cluster_assignments, sentiments, cluster_labels, notebook=False):
    """
    cluster_assignments: np.array which contains the cluster_ids per tweet
    sentiments: array which contains the sentiment for each tweet
    cluster_labels: dict which maps cluster_id to the cluster label/representation 
        this label/representation can be a string or a list of strings
    """
    if notebook:
        io.output_notebook()
    item_width = 300
    cluster_visual = []
    for cluster_id in np.unique(cluster_assignments):
        cluster_sentiments = np.array(sentiments)[cluster_assignments == cluster_id]
        counts, edges = np.histogram(cluster_sentiments, range=[0,1], bins=15)
        p = plotting.figure(title="", tools='', background_fill_color="#fafafa",
                           plot_width=item_width, plot_height=150)
        p.quad(top=counts, bottom=0, left=edges[:-1], right=edges[1:], fill_color="blue", line_color="white", alpha=0.5)
        p.y_range.start = 0
        p.title.align = 'center'
        p.yaxis.visible = False
        p.outline_line_color = None
        p.xaxis.major_tick_line_color = None

        p.xaxis.ticker = [0.2, 0.8]
        p.xaxis.major_label_overrides = {0.2: 'Negative', 0.8: 'Positive'}
        p.grid.grid_line_color="white"
        p.sizing_mode = 'scale_width'

        description = " - ".join(np.unique(cluster_labels[cluster_id]))
        html = widgets.Div(text="<p style='text-align:center; color:grey;'>" + description + "</p>", width=item_width)
        cluster_visual.append(layouts.column([p, html]))

    plotting.show(layouts.gridplot(cluster_visual, ncols=2))