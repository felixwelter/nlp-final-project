from nltk.sentiment.vader import SentimentIntensityAnalyzer

class VaderSentimentAnalyser():
  """VaterSentimentAnalyser which uses a ready to use library from nltk
  See this repo for the paper: https://github.com/cjhutto/vaderSentiment
  """

  def __init__(self):
    self.sid = SentimentIntensityAnalyzer()

  def get_sentiment(self, tweets):
    """Return the sentiment between 0 (negative) and 1 (positive)"""
    return [self.sid.polarity_scores(tweet)["compound"] / 2 + 0.5 for tweet in tweets]