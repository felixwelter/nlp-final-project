% !TeX spellcheck = de_DE_frami

\documentclass[]{scrreprt}
	% ===================================Praeambel==================================
	\usepackage{pdfpages}
	\usepackage{makecell}
	\usepackage{graphicx}
	\usepackage{float}
	\graphicspath{{resources/}} %Setting the graphicspath
	\renewcommand\theadfont{\bfseries}
	\renewcommand\theadgape{\Gape[4pt]}
	\renewcommand\cellgape{\Gape[4pt]}
	\usepackage{tabulary}
	\usepackage[utf8]{inputenc}
	\usepackage[english]{babel}
	
% ===================================Dokument===================================
\title{NLP Project Documentation}
\author{Christian Haase, Fabian Rausch, Felix Welter}
\date{28.01.2020}
% ================================Deckblatt-Muster==============================

\begin{document}
\newpage
\thispagestyle{empty}

\begin{titlepage}% {{{
	\begin{center}\Large
		University of Hamburg
		\vfill
		\includegraphics[width=1\textwidth]{twitter.jpg}
		\vfill
		\makeatletter
		{\Large\textsf{\textbf{\@title}}\par}
		Aggregating social media voices based on Twitter clusters
		\makeatother
		\vfill
		submitted by
		\par\bigskip
		\makeatletter
		{\@author} \par
		{\@date}
		\makeatother
	\end{center}
\end{titlepage}

\setcounter{secnumdepth}{3}
\tableofcontents

\chapter{Project objective}
The importance of social media and Twitter is high for political debates in the age of the @realDonaldTrump. Thus, there is a great interest among citizens, politicians and marketing departments to understand opinion formation on social media and Twitter. Among the publishing platforms in the Web, the microblogging service Twitter plays a key role. The data it produces consists of so-called tweets, these are mini-blog-entries of 280 Unicode characters. Since there are 500 million tweets per day on average, opinion and sentiment need to be analysed by automatic means.

\section{Research problems}
The research problem investigated is to find out whether it is possible to cluster the text content of Tweets about specific topics and to gauge the sentiment related to these clusters. This is meant to break down the very large content related to hashTags such as \# Iphone. The working hypothesis is that the smaller chunks which constitute the aggregated content reveal clusters of opinion with possible emotional sentiment attached. For example, related to the hashTag \# Iphone people may express opinions such as "love the look of the new \# IphoneX" or "Apple should unlock phones in terrorism cases. Hate Apple". While it is easy to find such realistic examples it is very difficult to put sentiment clustering of tweets into practice due to the very large amount of tweets.
\newline
In order to solve the problem, we broke it down into sub-problems that emerged from our literature analysis. We surveyed the literature on these two topics, namely thematic clustering and sentiment analysis of tweets, via a Journal-Publication analysis on SemanticScholar.org. 
\newline
\newline
\textbf{1. Problem: Definition and removal of textual noise
\newline}
Our literature analysis has revealed that most scientists consider noise as the key problem in any Twitter-analysis. (Lavanya 2020) While scientists agree that noise needs to be reduced, it is not clear what type of the data constitutes the noise exactly. Some authors are suggesting to remove stop-words, emojis and hash-tags. Other authors argue that hashTags provide valuable categories for analysis. Thus, our first research problem will be to identify and remove the "noise" from Twitter data streams, in order to get a good basis for the analysis of clusters and sentiments.
\newpage
\textbf{2. Problem: Best clustering algorithm and vectorization technique to get thematic clusters}
\newline
Despite the advancement of word-embeddings, in the realm of Twitter-Clustering Tf-IDF Sparse-Vector still is considered as the best standard solution. (Lavanya 2020) TF-IDF sparse vectors are supposedly best clustered with k-means clustering. However, the literature is not very detailed on these topics. Thus, our second research problem will be to follow up this lead and to analyse whether TF-IDF and kmeans are really the best means to cluster tweets.
\newline
\newline
\textbf{3. Problem: Robustness and Usefulness of sentiment analysis}
\newline
The advances in sentiment analysis have resulted in the development of robust sentiment analysers for tweets. The literature on this topic is broad, ranging form more traditional techniques to modern sentiment-classification with BERT. Here, the problen will be to analyse the robustness and usefulness of the sentiment analysis.
\newline
\newline


\chapter{Solution approach}


\section{Software architecture}

Figure \ref{fig_building_block_layer_1} illustrates the architecture of the software using the building block view. There are two main possibilities to construct the datapipeline. One can either use (1) a parallel stream for sentiment and clustering in order to use different de-noising and vectorization techniques or one can (2) use a serial pipeline. Due to the robustness of the sentiment analyser-module we decided to go with a serial pipeline up until the clusterer-module and focus on the correct de-noising and vectorization for the clusteirng part. After the clustering block, however, we split the labelling and the sentiment analysis task as these do not depend on each other. Each building block takes responsibility for one step of the data processing pipeline. At the borders moreover there are shown external systems which communicate with the application.

\begin{figure}[hbtp]
	\centering
	\includegraphics[width=1\textwidth]{building_block_view_layer_1.png}
	\caption[Top layer of the building block view]{Top layer of the building block view}
	\label{fig_building_block_layer_1}
\end{figure}

The pipeline starts by collecting data (top left) and finishes with different kinds of visualization (bottom). In the following sections each building block is explained in detail.

\subsection{Data Crawler}
The purpose of the data crawler is to get tweets for a specific topic and store them locally in a file formatted in a way that allows easy access from the Python application. For the purposes of this project, only the text content of the Tweet is needed. Therefore all other fields are removed.

\begin{itemize}
	\item \textbf{Input} The data crawler needs a json file containing Twitter credentials to use the API and a hashtag which will be searched on Twitter.
	\item \textbf{Output} The outcome of this step is a file named like the hashtag which contains the text of multiple Tweets separated by line breaks.
\end{itemize}

The data crawler is an independent part of the pipeline, which means it is runnable without the other steps. We run this script several times for different hashtags and saved them as described. The other steps use the output of this step and do not need to rerun it.

\subsection{Preprocessor}
The preprocessing step cleans the tweets that were saved in the data crawl step. Cleaning includes:
\begin{itemize}
	\item Lower case all words
	\item Remove all stopwords
	\item Remove hashtags ("\#") and mentions ("@")
	\item Remove quotation marks
	\item Remove dots
	\item Remove brackets
	\item Removing all words that occur less than x (ie. 5) times in the corpus
\end{itemize}
Not all of these steps are required for each use case. Parts of them are separated into different methods to use them when needed.

\begin{itemize}
	\item \textbf{Input} An array of Tweets
	\item \textbf{Output} An array of cleaned Tweets
\end{itemize}

\subsection{Vectorizer}
There are different vectorizers implemented to get a numeric representation of the Tweet contents.

\begin{itemize}
	\item Tf-idf/sparse vectors
	\item Fast text
	\item \textbf{ELMo} is a deep contextualized word representation. Each word representation depends on the entire context and moreover they combine all layers of a deep pre-trained neural network. Besides ELMo is character-based meaning it is able to use morphological clues to form robust representations for out-of-vocabulary words unseen in training \cite{ELMo}.
\end{itemize}

The interface of the vectorizers follow this setup:
\begin{itemize}
	\item \textbf{Input} Cleaned Tweets array
	\item \textbf{Output} Vector representation of Tweet contents
\end{itemize}

\subsection{Clusterer}
The clustering tries to find clusters in the Tweet vectors. The used methods are:

\begin{itemize}
	\item KMeans
	\item MeanShift
	\item Agglomerative Clustering
\end{itemize}

To improve the results of KMeans, we have various scores to assess the quality of the clusters. Using that allows to determine the best possible amount of clusters. (This is not straight-forward though). The metrics used were Silhouette (point-based score measuring internal cohesion and similiarity to nearest neighbour cluster), Calinski-Harabasz-Score (inner dispersion and distribution of all clusters), Davies-Bouldin-Score (centroid-based measure for internal cohesion and comparison to the most similiar cluster).

\subsection{Labeler}
There are two different labelers implemented:
\begin{itemize}
	\item LexRank
	\item Top 10
\end{itemize}
LexRank is a extractive summarizer which tries to find the most central sentence in a document cluster. Due to the missing amount of long and meaningful sentences LexRank struggles finding a central sentence. This problem increases with a more intensive cleaning of the Tweets as far as we were able to consider. \newline
Due to these problems, there is the second labeler (Top 10) implemented, which returns the 10 most frequent words. This one works without any problems thanks to the simplicity.

\subsection{Sentiment Analyzer}
For sentiment analysis there is VADER implemented \footnote{https://github.com/cjhutto/vaderSentiment}.

\subsection{Visualization}
We visualized the results using matplotlib and bokeh. We used downsampling for the fasttext- and tf-idf-vectors with principle component analysis (PCA).

\subsection{Third-party software}
\begin{itemize}
	\item numpy
	\item pandas
	\item lexrank
	\item sklearn
	\item matplotlib
	\item fasttext
	\item nltk
	\item tweepy
	\item langdetect
	\item bokeh
	\item tensorflow\_hub
	\item tensorflow (version 1.15.0)
	\item jsonpickle
\end{itemize}

\chapter{Project results}

\section{Example Data: Tweets}

We decided to develop our architecture with respect to example data from Twitter. We downloaded tweets related to the following hashtags:
\newline
\begin{itemize}
	\item IPhone (7190)
	\item Alexa (5469)
	\item Brexit (71827)
	\item Baseball (3739)
	\item Iran (35167)
	\item Trump (34991)
\end{itemize}
Out of these, we mainly used the Iphone-tweets because they were extremely noisy, "emotional", and promised to reveal different clusters and sentiments. A typical highly "emotional" Iphone-tweet read as follows:
\newline
\newline
Negative:
\newline
 'The way I see it is, if Apple refuses to unlock an iPhone in a terror investigation, then they are supporting terrorism.  Those buying their products are supporting terrorism. \#iphone \#Apple \#DOJ',
 \newline
 \newline
Positive:
\newline
'Create awesome custom faces for your \#AppleWatch. Download AWC Faces \#iphone \#app https://t.co/ctiK1X9Kda',
\newline
Within the 7000 Iphone tweets, there were more than 10.000 hashtags and (after cleaning) 28527 different words. In order to make this large set more manageable for the difficult cluster-analysis, we decided to use a sample of 1000 tweets. Thus, all experiments were finally conducted on the IPhone-Tweets[:1000]. 

\section{Problem 1: de-noising for clustering and sentiment-analysis}

The main method for the de-noising of the text-data was the above mentioned removal of stop-words etc. In addition, we analysed the effect that the removal of words that only occured x-times in the corpus had on the cluster-building and the sentiments. [The clustering and the sentiment is explained later on]. With the chosen number of 60 clusters, we observed that the average cluster quality improved with the threshold of 2 and stayed the same after. The sentiment-mean and the max-min sentiment scores stayed the same regardless of the removal of the threshold-words, indicating the robustness of the sentiment analyser. We decided to remove all words that occured less than three times in the corpus.


\begin{table}[H]
\begin{tabular}{llllllll}
Tf per Corpus & VocabSize & MaxCluster & Av Silhouette & Senti-Mean & Sent-Std & Sent-Max & Min \\
0             & 6706      & 60         & 0.06          & .60  & .19 & .98 & .02 \\
1             & 1984      & 63         & 0.06          & .61  & .17 & .98 & .06 \\
2             & 1171       & 44         & .13           & .60  & .14 & .96 & .02 \\
10            & 197       & 62         & .12           & .60  & .14 & .96 & .29 \\
20            & 86        & 62         & .12           & .60  & .14 & .96 & .29
\end{tabular}
\end{table}


After the removal of all words that occured less than 3 times in the corpus, the vocab-size was reduced to 1172 words from the original 6700. This sounds pretty small, but it was absolutely sufficient to understand, cluster and evaluate the tweets. The frequency of the words followed a power-law distribution:
\newline
\begin{itemize}
	\item iphone  :  1192
	\item apple  :  432
	\item android  :  144
	\item new  :  115
	\item ios  :  110
	\item ipad  :  105
	\item phone  :  91
	\item samsung  :  86
	\item tech  :  80
\end{itemize}
We also used another extreme denoising technique, namely removing all words and just keeping the hash-tags, but the results on clustering were not outstanding (they were good though). This required a parallel pipeline because the sentiment was run for this test standard-cleaned tweets. 


\section{Problem 2: clustering/vectorization}

The literature suggests to use TF-IDF. However, TF-IDF did not give us good results for clustering. The clusters were extremely uneven reflecting statistical "islands" created by high TF-IDF scores. We, thus, decided to vectorized with fastText, fastText with TF-IDF, TF-IDF and TF-IDF Hash-Tags only. We found  that fastText outperformed the other methods. We build the document-vectors for the fastText-embedding solutions with centroids. 

The strength of the fastText-solution was not visible from the first visualization of the document-vectors which appeared pretty close together. The visualization for TF-IDF was even worse with many  little dots strewn across the vector space. Both representations did not look very promising for clustering. There were no "natural" clusters that emerged in the 2D-visualisation.

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{resources/ftvecs}
	\caption[Top layer of the building block view]{Document-Centroids of FastText-Word-Embeddings}
	\label{ftvecs}
\end{figure}
Given the uneven distribution of the document vectors, it was not surprising that the clustering algorithms did not find clusters in the beginning. MeanShift and Agglomerative-Clustering, which offer automatic detection, did not find any clusters and the Silhouette Average was very low for many K-Means cluster-numbers indicating some problems with the clusters. However, by conducting a more detailed break-down of the three scores for cluster-candidates we were able to find good clusters at 60 and 44. We set the upper number for the clusters at 66 [2/3 of 100] as this seemed a manageable size which gave good results. We computed the scores for the three cluster-measures for all numbers of clusters. The Silhouette and Davies-Boudin scores were rather high due to the closeness and similiarity of the clusters. We obtained for the fast-text embedding the following results:

\begin{itemize}
	\item SA best  44  :  0.11010201254230478
	\item  SA worst  11  :  -0.014748700551768064
	\item  CH best  4  :  55.250385899578944
	\item  CH worst  64  :  16.951149226190655
	\item  DB best  65  :  1.7740207017694825
	\item  DB worst  3  :  3.4270839829065207
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=1\textwidth]{ftclusters}
	\caption[Top layer of the building block view]{Clusters-FastText}
	\label{ftclusters}
\end{figure}

The individual scores of each cluster reveal that there very good clusters in the dataset, but that the average Silhouette Score was rather low doe to some "noisy" bad clusters strewn in between (visible as those with negative Silhouette Scores). The density of the vector-embeddings further dragged the Calinski-Harabasz-Score down. The clusters we obtained with fastText were well distributed. The number of tweets per cluster was as follows for the 44 clusters:
\begin{itemize}
	\item count    44.000000
	\item mean     22.727273
	\item std      18.399145
	\item min       2.000000
	\item max      84.000000
\end{itemize}

The results with fastText were the best we could obtain. The distribution with TF-IDF resulted in uneven clusters. The heuristic method used to find the optimal cluster number was based on the best of the three scores and an in-depth breakdown of the Silhouette-Scores for each cluster. The reason why FastText performed better than TF-IDF sparse vectors was the additional semantic clustering. It kept the clusters tight together, but also clearly seperable with special words. However, neither TF-IDF nor FastText clustering were able allocate all "unlocking-debate"-tweets into the "unlocking"-cluster. This underlines, the problems of obtaining high quality clusters in very noisy tweet-streams.


\section{Problem 3: sentiment}
The good clustering results could be verified by the identification of topics and with useful sentiments. The negative "terrorism"-case debate came through as a cluster and the positive ones about apps, music and new phones as well.  Two example-clusters that we identified and could label with sentiment scores:
\newline
\newline
Positive:
\newline
['full', 'edge', 'iphone', 'check', 'weekend', 'weekend', 'flash', 'sale', 'xx', 'protect'] [MOST POSITIVE CLUSTER-SENTIMENT AVERAGE AT 0,81]
\newline
\newline
Negative
\newline
Cluster  13  top 10:  ['iphone', 'apple', 'unlock', 'fbi', 'use', 'privacy', 'terrorism', 'phone', 'watch', 'shooter'] [MOST NEGATIVE SENTIMENT-AVERAGE AT 0.49]
\newline


\section{Main result: best approach to sentiment-clustering of tweets}
Research on clustering tweets has so far emphasized the strength of TF-IDF. However, our research has shown that fastText-vectorization with a word-corpus-threshold of 2 and a k-means "superscore" calculated from all three measures performed better. It identified the thematic clusters and evaluated them with a clear sentiment analysis. Thus, our research has advanced the research on tweet-clustering and sentiment analysis. The clusters were good, but - due to the noise - not very good. Some obvious tweets were missed by the clustering algorithms regardless of the vectorization employed. The open question is to what extent and how these results can be improved, generalized and scaled further. For this, more tests and research would be necessary.



\begin{thebibliography}{9}
	\bibitem{bojanowski_enriching} 
	P. Bojanowski, E. Grave, A. Joulin, T. Mikolov. 
	\textit{Enriching Word Vectors with Subword Information (PrePrint)}.
	2016.	
	
	\bibitem{erkan_lexrank} 
	G. Erkan and D. R. Radev.
	\textit{LexRank: Graph-based Lexical Centrality as Salience in Text Summarization}.
	2004.
	
	\bibitem{gupta_tweetnormalize} 
	I. Gupta, N. Joshi. 
	\textit{Tweet normalization: A knowledge based approach}.
	2017 International Conference on Infocom Technologies and Unmanned Systems (Trends and Future Directions) (ICTUS), pp. 157–162, 2017.
		
	\bibitem{hutto_sentiment} 
	C.J. Hutto \& E.E. Gilbert
	\textit{VADER: A Parsimonious Rule-based Model for Sentiment Analysis of Social Media Text}. Eighth International Conference on Weblogs and Social Media (ICWSM-14). 2014
	
	\bibitem{lavanya_tweets} 
	Lavanya, P. G.; Kouser, K.; Suresha, Mallappa
	\textit{Efficient Pre-processing and Feature Selection for Clustering of Cancer Tweets}.
	 In: Sabu M. Thampi, Ljiljana Trajkovic, Sushmita Mitra, P. Nagabhushan, Jayanta Mukhopadhyay, Juan M. Corchado et al. (Hg.): Intelligent Systems, Technologies and Applications, Bd. 910. Singapore: Springer Singapore (Advances in Intelligent Systems and Computing), S. 17–37. 2020.
	
	
	\bibitem{ELMo} 
	M. E. Peters, M. Neumann, M. Iyyer, M. Gardner, C. Clark, K. Lee, L. Zettlemoyer
	\textit{ELMO: Deep contextualized word representations (PrePrint)}.
	http://arxiv.org/pdf/1802.05365v2. 2018.
	
	\bibitem{sculley_clustering} 
	D. Sculley 
	\textit{Web-Scale K-Means Clustering}.
	In: International Conference on World Wide Web; WWW '10. New York, NY: ACM Digital Library; ACM, S. 1177. 2010.
	
	
\end{thebibliography}

\end{document}
