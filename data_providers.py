from sklearn.datasets import fetch_20newsgroups

class DummyDataProvider():
  """DummyDataProvider which simply returns hard coded tweets"""
  def load_data(self):
    return [
      "This is about cats #cats ",
      "Feel th stars and the sky #feeling #living ",
      " Lets do it all together #community"
    ]

class CachedSmallDataProvider():
  """CachedSmallDataProvider loads a small amount of tweets saved in this repo"""
  def __init__(self, topic):
    self.topic = topic

  def load_data(self):
    return [line.rstrip('\n') for line in open("tweets/" + self.topic + ".txt", encoding="utf8")]


if __name__ == "__main__":
  provider = CachedSmallDataProvider("brexit")
  print(provider.load_data())
