import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
from collections import defaultdict

class DummyPreprocessor():
  """DummyPreprocessor which does nothing else than stripping whitespace"""

  def clean_tweets(self, tweets):
    return [tweet.strip() for tweet in tweets]


class SurfaceCleaner():

  def clean_tweets(self, tweets):
    return Cleaner().clean_tweetsFast(tweets)
        


class Cleaner():
  """does all kinds of cool stuff ;) """
  # param : lowerTfThreshold: removes all terms with a frequency <= threshold : standard = 1

  def __init__(self):
    self.stop_words = set(stopwords.words('english'))
    self.stopwords_extra = ("there", "is", "the", "an", "rt", "that", "from", "this", "of",
                     "he", "she", "to", "you", "in", "it", "was", "is",
                     "on", "been", "have", "has", "https", "amp", "ha")
    for word in self.stopwords_extra:
      self.stop_words.add(word)
    

  def clean_tweetsFast(self, tweets):
    # documents
    documentsCap = tweets
    documents = []

    # convert to lower // #remove all . from words
    # DOES remove # and @
    for document in documentsCap:
      document = self.cleanTextAndTagSymbols(document)
      document = document.lower()
      docTemp = ""
      
      for word in document.split(" "):
        if word not in self.stop_words:
          docTemp += word + " "
      documents.append(docTemp)

    return documents

  def clean_tweets(self, tweets, tfThreshold):
    # param : lowerTfThreshold: removes all terms with a frequency <= threshold : standard = 1
    # returns: vocabDicFreq : Dictionary with ORIGNAL lowercase words and frequencies {word:frequency}
    # returns: vocabDic : Dictionary with REDUCED index of words {index:word} for bag-of-words-vectors
    # returns cleaned tweets without words that only occur once in all tweets

    # documents
    documentsCap = tweets
    documents = []

    # convert to lower // #remove all . from words
    # DOES remove # and @
    for document in documentsCap:
      document = self.cleanTextAndTagSymbols(document)
      document = document.lower()
      documents.append(document)

    # put vocab into vocab dictionary with frequencies {word:frequency}
    # does NOT use word-Tokenizer because the removal of @ and # ist NOT wanted here
    vocabDicFreq = defaultdict(int)
    for document in documents:
      words = document.split(" ")
      for word in words:
        vocabDicFreq[word] += 1

    # create indexed vocabDic by cleaning vocab: {index:word}
    # (1) remove all words of len=1 (?) (2) remove stopwords (3)remove words of freq > lowerFrew
    vocabDic = {}
    indexNew = 0
    for wort in list(vocabDicFreq.keys()):
      if (len(wort) != 1) and (wort not in self.stop_words) and (vocabDicFreq[wort] > tfThreshold):
        vocabDic[indexNew] = wort
        indexNew += 1

    listeWorte = list(vocabDic.values())
    vocabDicFreq = defaultdict(int)
    for document in documents:
      words = document.split(" ")
      for word in words:
        if word in listeWorte:
          vocabDicFreq[word] += 1

    # create new tweets
    tweetsReturn = []
    wortList = list(vocabDic.values())
    for document in documents:
      tweetSingle = ""
      words = word_tokenize(document)
      for word in words:
        if word in wortList:
          tweetSingle += (word + " ")
      if tweetSingle != "":
        tweetsReturn.append(tweetSingle[:-1])
      else:
        tweetsReturn.append(" ")
    return vocabDicFreq, vocabDic, tweetsReturn;

  def cleanTextAndTagSymbols(self, text):
    # removes # and @
    document = self.cleanTextButLeaveTagSymbols(text)
    document = document.replace("@", "")
    document = document.replace("#", "")
    return document


  def cleanTextButLeaveTagSymbols(self, text):
    # DOES NOT REMOVE # and @
    document = text
    document = document.replace('“', '')
    document = document.replace('”', '')
    document = document.replace('’', ' ')
    document = document.replace('‘', '')
    document = document.replace('…', '')
    document = document.replace('.', '')
    document = document.replace('(', '')
    document = document.replace(')', '')
    document = document.replace("'", "")
    document = document.replace("``", "")
    return document

  def createDictionariesHashTags(self, hashTagsPerTweetArray):
    vocabDicFreq = defaultdict(int)
    for tag in hashTagsPerTweetArray:
      vocabDicFreq[tag] += 1

    # create indexed vocabDic by cleaning vocab: {index:word}
    # (1) remove all words of len=1
    vocabDic = {}
    indexNew = 0
    for wort in list(vocabDicFreq.keys()):
      if (len(wort) != 1):
        vocabDic[indexNew] = wort
        indexNew += 1
    return vocabDic, vocabDicFreq

if __name__ == "__main__":
  preproc = DummyPreprocessor()
  print(preproc.clean_tweets([
    "test me now ",
    " test"
    ]))