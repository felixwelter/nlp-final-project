import numpy as np
import tensorflow_hub as hub
import tensorflow as tf
import fasttext
from nltk import word_tokenize
import math
from sklearn.feature_extraction.text import TfidfVectorizer


class DummyVectorizer():
  """DummyVectorizer which returns random arrays"""

  def vectorize(self, tweets):
    return np.array([np.random.random(size=20) for tweet in tweets])

class FastTextMeanVectorizer():
  """FastTextVectorizer uses fasttext model and average word vecs to calculate tweet vec"""
  def __init__(self, path_to_model="cc.en.300.bin"):
    self.ft_model = fasttext.load_model(path_to_model)

  def vectorize(self, tweets):
    return np.array([self.vectorize_single_tweet(tweet) for tweet in tweets])

  def vectorize_single_tweet(self, tweet):
    words = word_tokenize(tweet)
    return np.mean(np.array([self.ft_model[word] for word in words]), axis=0)


class ElmoVectorizer():

  def __init__(self):
    self.elmo = hub.Module("https://tfhub.dev/google/elmo/3")
    
  def vectorize(self, tweets):
    embeddings = self.elmo(tweets, signature="default", as_dict=True)["default"]
    with tf.compat.v1.Session() as sess:
      sess.run(tf.compat.v1.global_variables_initializer())
      sess.run(tf.compat.v1.tables_initializer())
      x = sess.run(embeddings)
      return x
    

class FastTextTfIdfMeanVectorizer():
  def __init__(self, path_to_model="cc.en.300.bin"):
    self.ft_model = fasttext.load_model(path_to_model)

  def vectorize(self, docSparseTfIds, vocabDic):
    return np.array([self.vectorize_single_tweet(docTfIdfVector, vocabDic) for docTfIdfVector in docSparseTfIds.values()])

  def vectorize_single_tweet(self, docTfIdfVector, vocabDic):
    tfidfCentroid = [0 for i in range(300)]
    count = 1
    for element in range(len(docTfIdfVector)):
      if docTfIdfVector[element] != 0:
        count += 1
        tfidfCentroid += docTfIdfVector[element]*self.ft_model[vocabDic[element]]
    # count creates mean for centroid (tfidfCentroid/count)
    return (tfidfCentroid/count)


class FastTextMeanVectorizerVocabDic():
  def __init__(self, path_to_model="cc.en.300.bin"):
    self.ft_model = fasttext.load_model(path_to_model)

  def vectorize(self, docSparseTfIds, vocabDic):
    return np.array([self.vectorize_single_tweet(docTfIdfVector, vocabDic) for docTfIdfVector in docSparseTfIds.values()])

  def vectorize_single_tweet(self, docTfIdfVector, vocabDic):
    centroid = [0 for i in range(300)]
    count = 1
    for element in range(len(docTfIdfVector)):
      if docTfIdfVector[element] != 0:
        count += 1
        centroid += self.ft_model[vocabDic[element]]
    # count creates mean for centroid (tfidfCentroid/count)
    return (centroid/count)


class TfIdfVectorizer():

  def vectorize(self, vocabDic, documents):
    vocabSize = len(vocabDic)
    # create sparse vectors with tf-idf form {docId:[sparse vector]}
    # first round Boolean
    docSparseBoolean = {}
    for indexDoc in range(len(documents)):
      vector = [0 for i in range(vocabSize)]
      words = documents[indexDoc].split(" ")
      for indexWord in range(vocabSize):
        if vocabDic[indexWord] in words:
          vector[indexWord] = 1
      docSparseBoolean[indexDoc] = vector
    # print(docSparseBoolean[0])
    # second round tf
    docSparseTf = {}
    for indexDoc in range(len(documents)):
      vector = [0 for i in range(vocabSize)]
      words = documents[indexDoc].split(" ")
      for indexWord in range(vocabSize):
        for word in words:
          if vocabDic[indexWord] == word:
            vector[indexWord] += 1
      docSparseTf[indexDoc] = vector
    # print(docSparseTf[0])
    # third round tf - idf (idf = log (len(documents)/occurences))
    # step 1 create df value (id aus vocabDic: occurence in number of docs)
    vocabDicDf = {}
    for i in range(vocabSize):
      vocabDicDf[i] = 0
    for indexDoc in range(len(documents)):
      words = documents[indexDoc].split(" ")
      for indexWord in range(vocabSize):
        if vocabDic[indexWord] in words:
          vocabDicDf[indexWord] += 1
    # step 2 create tf-idf sparse vector multiply each word in tf-vector with idf value of this word
    docSparseTfIdf = {}
    for indexDoc in range(len(documents)):
      vectorTfIdf = docSparseTf[indexDoc]
      for indexWord in range(vocabSize):
        if (vocabDicDf[indexWord] != 0):
          vectorTfIdf[indexWord] = vectorTfIdf[indexWord] * (math.log(len(documents) / vocabDicDf[indexWord]))
      docSparseTfIdf[indexDoc] = vectorTfIdf
    return docSparseTfIdf

  def vectorizeFast(self, documents):
    # NOTWORKING in Pipeline - randomizes words in other
    docSparseTfIdf = {}
    dicVocab = {}
    vectorizer = TfidfVectorizer()
    X = vectorizer.fit_transform(documents)
    arrayarray = X.toarray()
    for index in range(len(arrayarray)):
      docSparseTfIdf[index] = arrayarray[index]
    nameList = vectorizer.get_feature_names()
    for index in range(len(nameList)):
      dicVocab[index] = nameList[index]
    return X, docSparseTfIdf, dicVocab


    

if __name__ == "__main__":
  vectorizer = FastTextMeanVectorizer()
  vecs = vectorizer.vectorize([
    "Helo world",
    "test me now"
    ])
  print(vecs)