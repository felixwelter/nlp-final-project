"""
This file contains cluster labeling methods in a more general
sense, which means it can include anything to "describe" a cluster
by means of summarization, naming or color coding.

Because of the diverse functions set of these classes, 
contents of this file are not bound to a common API.
"""

import lexrank
import numpy as np
from preprocessors import Cleaner

class LexRankSummarizer():
    def get_cluster_summary(self, sents, count=1):
        lxr = lexrank.LexRank(sents, lexrank.STOPWORDS["en"])
        scores = lxr.rank_sentences(sents, threshold=.1)
        summary_indices = np.argsort(scores)[::-1]
        return [sents[i] for i in summary_indices[:count]]

class TopTenWords():
  def get_cluster_summary(self, sents, count=1):
    liste1 = sents
    cleaner = Cleaner()
    vocabDicFreq2, vocabDic2, clean_tweets2 = cleaner.clean_tweets(liste1, 0)
    sortedDicFreq = {k: v for k, v in sorted(vocabDicFreq2.items(), key=lambda item: item[1], reverse=True)}
    liste = list(sortedDicFreq.keys())
    return liste[:10]