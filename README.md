# NLP Final Project

## Basic Usage
See the 'Pipeline Usage' Notebook for a simple example of how 
to create a pipeline. Once a pipeline is created, components 
can be replaced before executing. Set the corresponding 
instance variable to do so. 

See the following paragraphs on what to install to run
the program.

## Dependencies
Run 'pip install -r requirements.txt'. 

To ensure that the notebook can be used, run 'pip install jupyterlab'

## NLTK Data 
It is possible that NLTK shows an error about missing files, 
libraries or alike. Follow the instructions on this page
https://www.nltk.org/data.html to download the resources. 
Alternativly just execute 'nltk.download("all")'.

## Capturing twitter streaming data
Before you start retrieving new data: several example files are 
stored in the tweets folder to get started.

Get twitter api credentials and update the file
twitter_stream_capture.py accordingly. 
Run 'python tweet_capture.py desired_topic' 
and replace desired_topic with the filter you 
want to apply. 
Samples of the gathered data is displayed on the console
and all data is saved to the tweets folder.
The file also contains a stream listener to retrieve 
a live tweet stream using the twitter streaming api.

## Downloading word vectors
For the fasttext embedding downloading the english word vector bin file
from this website https://fasttext.cc/docs/en/crawl-vectors.html or 
via this direct link: 
https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.en.300.bin.gz
Alternativly you can use the file download_word_vectors.sh.
The program expects the file to be in the project root. 
If the file needs to be loaded from somewhere else, 
specify the path in the constructor. 

## Known (technical) issues
The ELMo vectorizer does not handle large amounts of tweets well, since
it needs to allocate too much RAM for the conversion. 

LexRank can only be applied if there are enough documents within a 
tweet cluster. "Enough" is not a fixed number in this case. Instead
LexRank needs enough word information to select fitting sentences. 
Below 1000 tweets within a group is generally to few, while groups 
with more than 3000 tweets seem to cause no problems. 

The programm execution takes too long to run it in a live setup. 
This also goes for the twitter API. However using the streaming api
and enough computational resources a setup where incremental updates
to the data and clusters (and therefore the visualization) should be possible. 

