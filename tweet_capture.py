import tweepy
import json
import sys
import jsonpickle
from langdetect import detect as detect_lang
from langdetect.lang_detect_exception import LangDetectException

class MyStreamListener(tweepy.StreamListener):

  def setup(self):
    self.tweet_count = 0
    self.output_file = open("tweets/"+ sys.argv[1] +".txt", "a")

  def on_status(self, status):
    tweet = status.text
    if tweet.strip == "":
      return
    try:
      if detect_lang(tweet) != "en":
        return
    except LangDetectException:
      return
    tweet = tweet.replace("\n", " ")
    self.tweet_count = self.tweet_count + 1
    self.output_file.write(tweet)
    self.output_file.write("\n")
    if self.tweet_count % 50 == 1:
      print(self.tweet_count)
      print(tweet)
    if self.tweet_count > 10000:
      exit()


  def on_error(self, status_code):
    print(status_code)
    if status_code == 420:
      return False

class TopicCrawler():

  def crawl(self, hashtag):
    # For the explanation of these fields see: https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets
    tweets_per_query = 100
    max_tweets = 1000000
    output_file = "tweets/" + hashtag + "-extended.txt"
    since_id = None
    max_id = -1
    tweet_count = 0
    found_tweets = True
    tweet_filter = " AND -filter:retweets"
    prefix = "#"

    with open(output_file, 'w') as f:
      print("Downloading tweets for hashtag: " + hashtag)
      while(tweet_count < max_tweets and found_tweets):
        try:
          if(max_id <= 0):
            if(not since_id):
              new_tweets = api.search(q = prefix + hashtag + tweet_filter, count = tweets_per_query, lang = "en", tweet_mode = 'extended')
            else:
              new_tweets = api.search(q = prefix + hashtag + tweet_filter, count = tweets_per_query, lang = "en", tweet_mode = 'extended', since_id = since_id)
          else:
            if(not since_id):
              new_tweets = api.search(q = prefix + hashtag + tweet_filter, count = tweets_per_query, lang = "en", tweet_mode='extended', max_id = str(max_id - 1))
            else:
              new_tweets = api.search(q = prefix + hashtag + tweet_filter, count = tweets_per_query, lang = "en", tweet_mode='extended', max_id = str(max_id - 1), since_id = since_id)

          if(not new_tweets):
            print("No more tweets found")
            found_tweets = False
          else:
            for tweet in new_tweets:
              f.write(jsonpickle.encode(tweet._json, unpicklable = False) + '\n')
              tweet_count += 1
              print("Successfully downloaded {0} tweets".format(tweet_count))
              max_id = new_tweets[-1].id

        except tweepy.TweepError as e:
          print(str(e))
          found_tweets = False

  def extract_useful_fields(self, hashtag):
    for line in open("tweets/" + hashtag + "-extended.txt", 'r'):
      with open("tweets/" + hashtag + ".txt", "a+") as out_file:
        out_file.write(json.loads(line)["full_text"].replace("\n", " ") + "\n")
    out_file.close()

creds = json.load(open('../twitter_creds.json'))
auth = tweepy.OAuthHandler(creds["consumer_key"], creds["consumer_secret"])
auth.set_access_token(creds["access_token_key"], creds["access_token_secret"])
api = tweepy.API(auth)

# Crawl data for topic
topicCrawler = TopicCrawler()
topicCrawler.crawl(sys.argv[1])
topicCrawler.extract_useful_fields(sys.argv[1])


# Crawl live data using Stream Listener
#myStreamListener = MyStreamListener()
#myStreamListener.setup()
#myStream = tweepy.Stream(auth=api.auth, listener=myStreamListener)
#myStream.filter(track=[sys.argv[1]])
